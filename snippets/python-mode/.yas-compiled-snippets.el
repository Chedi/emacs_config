;;; Compiled snippets and support files for `python-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'python-mode
                     '(("war" "with self.assertRaises(${1: Exception}):\n    $0\n" "with_assert_raises" nil
                        ("test")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/with_assert_raises" nil nil)
                       ("srepr" "def __str__(self) -> str:\n    return self.__repr__()\n$0" "srepr" nil nil
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/srepr" nil nil)
                       ("spm" "\"\"\"\n.. module:: ${1: module_name}\n   :platform: Unix\n   :synopsis: ${2: module_doc}\n\n.. moduleauthor:: chedi toueiti <chedi.toueiti@gmail.com>\n\"\"\"$0" "sphinx_module" nil
                        ("sphinx")
                        nil "/home/chedi/.emacs.d/snippets/python-mode/sphinx_module" nil nil)
                       ("rpdb" "from rpdb import Rpdb\nRpdb().set_trace()\n" "rpdb_set_trace" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/rpdb" nil nil)
                       ("ripdb" "import ripdb\nripdb.set_trace()\n" "ripdb_set_trace" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/ripdb" nil nil)
                       ("rdb" "from celery.contrib import rdb\nrdb.set_trace()\n" "rdb_set_trace" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/rdb" nil nil)
                       ("pdb" "import pdb\npdb.set_trace()$0" "pdb_set_trace" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/pdb" nil nil)
                       ("noqa" "# noqa # pylint: disable=unused-import" "noqa" nil nil nil "/home/chedi/.emacs.d/snippets/python-mode/noqa" nil nil)
                       ("ity" "# type: ignore" "ignore_type" nil
                        ("general")
                        nil "/home/chedi/.emacs.d/snippets/python-mode/ignore_type" nil nil)
                       ("fri" "from ${1:package} import ${2:module}$0" "from_import" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/from_import" nil nil)
                       ("__main__" "if __name__ == \"__main__\":\n    $0" "__main__" nil
                        ("general")
                        ((yas-indent-line 'fixed))
                        "/home/chedi/.emacs.d/snippets/python-mode/__main__" nil nil)))


;;; Do not edit! File generated at Wed Aug 26 13:15:32 2020
