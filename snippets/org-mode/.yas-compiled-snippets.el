;;; Compiled snippets and support files for `org-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'org-mode
                     '(("title" "#+TITLE: ${1:title}" "title"
                        (=
                         (current-column)
                         5)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/title.yasnippet" nil nil)
                       ("tkz" "#+CAPTION: $1\n#+LABEL: ${2:\"waiting\"$(unless yas/modified-p (reftex-label \"figure\" 'dont-insert))}\n#+ATTR_HTML: alt=\"$1\" width=\"$3%\"\n#+ATTR_LATEX: width=0.$3\\textwidth\n#+ATTR_ODT: (:scale 0.$3)\n#+HEADERS: :imagemagick yes :iminoptions -density 300 -resize 400\n#+HEADERS: :packages '((\"\" \"tikz\") (\"\" \"tkz-berge\")) :border 1pt\n#+BEGIN_SRC latex :file ${2:$(substring yas/text 4)}.png\n  \\begin{tikzpicture}\n    ${0}\n  \\end{tikzpicture}\n#+END_SRC\n" "tikz-figure"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/tikz-figure.yasnippet" "C-c y z" nil)
                       ("thm" "# <<$1>>\n#+BEGIN_THEOREM\n#+latex: \\label{${1:\"waiting\"$(unless yas/modified-p (reftex-label \"theorem\" 'dont-insert))}}%\n$0\n#+END_THEOREM" "theorem"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/theorem.yasnippet" "C-c y t" nil)
                       ("text" "#+TEXT: ${1:text}" "text"
                        (or
                         (=
                          (current-column)
                          4)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/text.yasnippet" nil nil)
                       ("sta" "#+STARTUP: ${1:options}" "startup"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/startup.yasnippet" nil nil)
                       ("sb" "#+NAME: ${1:name}\n#+BEGIN_SRC ${2:language}\n  $3\n#+END_SRC\n" "#+srcname:..#+begin_src...#+end_src"
                        (or
                         (=
                          (current-column)
                          2)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/sourceblock.yasnippet" "C-c y s" nil)
                       ("sh" "#+NAME: $1\n#+BEGIN_SRC sh\n$0\n#+END_SRC\n" "shell (shell)"
                        (=
                         (current-column)
                         2)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/shell.yasnippet" nil nil)
                       ("py" "#+NAME: $1\n#+BEGIN_SRC python\n$0\n#+END_SRC\n" "python"
                        (=
                         (current-column)
                         2)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/python.yasnippet" nil nil)
                       ("pro" "# <<$1>>\n#+BEGIN_PROPOSITION\n#+latex: \\label{${1:\"waiting\"$(unless yas/modified-p (reftex-label \"proposition\" 'dont-insert))}}%\n$0\n#+END_PROPOSITION" "proposition"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/proposition.yasnippet" "C-c y p" nil)
                       ("properties" " :PROPERTIES:\n :VISIBILITY:folded:\n :END:\n" "properties folded"
                        (=
                         (current-column)
                         10)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/propertiesfolded.yasnippet" nil nil)
                       ("prf" "#+BEGIN_PROOF\n$0\n#+END_PROOF" "proof"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/proof.yasnippet" nil nil)
                       ("opt" "#+OPTIONS: ${0}" "options"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/options.yasnippet" "C-c y o" nil)
                       ("lem" "# <<$1>>\n#+BEGIN_LEMMA\n#+LATEX: \\label{${1:\"waiting\"$(unless yas/modified-p (reftex-label \"lemma\" 'dont-insert))}}%\n$0\n#+END_LEMMA" "lemma"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/lemma.yasnippet" "C-c y l" nil)
                       ("lhe" "#+LATEX_HEADER: ${1:\\usepackage{$2}}\n" "LaTeX header"
                        (=
                         (current-column)
                         3)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/latexheader.yasnippet" nil nil)
                       ("lcl" "#+LATEX_CLASS: ${1:$$(yas/choose-value '(\"report\" \"book\" \"article\"))}\n" "LaTeX class"
                        (=
                         (current-column)
                         3)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/latexclass.yasnippet" nil nil)
                       ("lan" "#+LANGUAGE: ${1:$$(yas/choose-value '(\"en\" \"es\"))}\n" "language"
                        (=
                         (current-column)
                         3)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/language.yasnippet" nil nil)
                       ("keys" "#+KEYWORDS: ${1:keywords}\n" "keywords" nil nil nil "/home/chedi/.emacs.d/snippets/org-mode/keywords.yasnippet" nil nil)
                       ("image" "#+NAME   : $1\n#+CAPTION: $2\n[[$3]]\n$0\n" "image" nil nil nil "/home/chedi/.emacs.d/snippets/org-mode/image.yasnippet" nil nil)
                       ("fig" "#+attr_latex: width=$1\\textwidth\n#+ATTR_HTML: width=\"$2%\"\n#+caption: $3\n`(org-insert-link '(4))`\n$0\n" "figure"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/figure.yasnippet" "C-c y f" nil)
                       ("eqn" "# <<$1>>\n\\begin{equation}\n\\label{${1:\"waiting for reftex-label call...\"$(unless yas/modified-p (reftex-label nil 'dont-insert))}}%\n$0\n\\end{equation}\n" "equation"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/equation.yasnippet" "C-c y e" nil)
                       ("email" "#+EMAIL: ${1:`user-mail-address`}\n" "email"
                        (or
                         (=
                          (current-column)
                          5)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/email.yasnippet" nil nil)
                       ("el" "#+NAME: $1\n#+BEGIN_SRC emacs-lisp\n$0\n#+END_SRC" "el (emacs-lisp)"
                        (=
                         (current-column)
                         2)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/el.yasnippet" nil nil)
                       ("dit" "#+BEGIN_SRC ditaa ${1:export-file-name} -r -s -e\n${0}\n#+END_SRC\n" "ditaa"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/ditaa.yasnippet" nil nil)
                       ("desc" "#+LANGUAGE: ${1:description}\n" "description" nil nil nil "/home/chedi/.emacs.d/snippets/org-mode/description.yasnippet" nil nil)
                       ("def" "# <<$1>>\n#+BEGIN_DEFINITION\n#+LATEX: \\label{${1:\"waiting\"$(unless yas/modified-p (reftex-label \"definition\" 'dont-insert))}}%\n$0\n#+END_DEFINITION" "definition"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/definition.yasnippet" "C-c y d" nil)
                       ("cor" "# <<$1>>\n#+BEGIN_COROLLARY\n#+latex: \\label{${1:\"waiting\"$(unless yas/modified-p (reftex-label \"corollary\" 'dont-insert))}}%\n$0\n#+END_COROLLARY" "corollary"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/corollary.yasnippet" "C-c y c" nil)
                       ("bbb" "#+TITLE:        ${1}\n#+INDEX:        ${2}\n#+AUTHOR:       ${3}\n#+STARTUP:      inlineimages latexpreview showeverything\n#+FILETAGS:     ${4}\n#+KEYWORDS:     ${5}\n#+DESCRIPTION:  ${6}\n#+EXCLUDE_TAGS: Excerpt\n\n* Excerpt                                                           :Excerpt:\n\n  $0" "blog post headers" nil nil nil "/home/chedi/.emacs.d/snippets/org-mode/blog_post_header.yasnippet" nil nil)
                       ("bimg" "#+CAPTION:   ${1:caption}\n#+ATTR_HTML: :title ${2:title}\n#+ATTR_HTML: :data-lightbox $2\n#+ATTR_HTML: :data-title $2\n#+ATTR_HTML: :width 800px\n[[${3:image_link}]]\n$0\n" "blog image" nil nil nil "/home/chedi/.emacs.d/snippets/org-mode/blog-image.yasnippet" nil nil)
                       ("block" "#+BEGIN_$1 $2\n  $0\n#+END_$1\n" "#+begin_...#+end_"
                        (or
                         (=
                          (current-column)
                          5)
                         (=
                          (current-column)
                          0))
                        nil
                        ((yas/indent-line 'fixed)
                         (yas/wrap-around-region 'nil))
                        "/home/chedi/.emacs.d/snippets/org-mode/block.yasnippet" "C-c y b" nil)
                       ("bh" "#+NAME: $1\n#+BEGIN_SRC bash\n$0\n#+END_SRC\n" "shell (bash)"
                        (=
                         (current-column)
                         2)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/bash.yasnippet" nil nil)
                       ("aut" "#+AUTHOR: ${1:`user-full-name`}\n" "author"
                        (or
                         (=
                          (current-column)
                          3)
                         (=
                          (current-column)
                          0))
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/author.yasnippet" "C-c y a" nil)
                       ("atl" "#+attr_latex: ${1:width=$2\\textwidth}\n" "attr_latex"
                        (=
                         (current-column)
                         3)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/attr_latex.yasnippet" nil nil)
                       ("ath" "#+ATTR_HTML: alt=\"$1\" img class=\"aligncenter\"\n" "attr_html"
                        (=
                         (current-column)
                         3)
                        nil nil "/home/chedi/.emacs.d/snippets/org-mode/attr_html.yasnippet" nil nil)))


;;; Do not edit! File generated at Wed Aug 26 13:15:32 2020
