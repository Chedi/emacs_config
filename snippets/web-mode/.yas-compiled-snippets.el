;;; Compiled snippets and support files for `web-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'web-mode
                     '(("table" "<table class=\"table $1\"${2: id=\"$3\"}>\n  <thead>\n    $0\n  </thead>\n  <tbody>\n  </tbody>\n  <tfoot>\n  </tfoot>\n</table>" "table" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/table.yasnippet" nil nil)
                       ("row" "<div class=\"row\">\n  $0\n</div>" "row" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/row.yasnippet" nil nil)
                       ("panel" "<div class=\"panel $1\">\n  <div class=\"panel-heading\">$2</div>\n  <div class=\"panel-body\">\n    $0\n  </div>\n</div>" "panel" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/panel.yasnippet" nil nil)
                       ("modal" "<div class=\"modal fade\" \n     id=\"$1\" \n     tabindex=\"-1\" \n     role=\"dialog\" \n     aria-labelledby=\"$2\" \n     aria-hidden=\"true\">\n  <div class=\"modal-dialog\">\n    <div class=\"modal-content\">\n      <div class=\"modal-header\">\n        <button type=\"button\" \n		class=\"close\" \n		data-dismiss=\"modal\" \n		aria-hidden=\"true\">&times;</button>\n        <h4 class=\"modal-title\" id=\"$2\">$3</h4>\n      </div>\n      <div class=\"modal-body\">\n        $0\n      </div>\n      <div class=\"modal-footer\">\n        <button type=\"button\" \n                class=\"btn btn-default\" \n                data-dismiss=\"modal\">${5:Close}</button>\n        <button type=\"button\" \n		class=\"btn btn-primary\">${6:Save}</button>\n      </div>\n    </div>\n  </div>\n</div>" "modal" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/modal.yasnippet" "direct-keybinding" nil)
                       ("icon" "<span class=\"glyphicon glyphicon-$1\"></span>$0" "icon" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/icon.yasnippet" nil nil)
                       ("div" "<div class=\"$1\"${2: id=\"$3\"}>\n  $0\n</div>" "div" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/div.yasnippet" nil nil)
                       ("container" "<div class=\"container\">\n  $0\n</div>" "container" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/container.yasnippet" nil nil)
                       ("button" "<button type=\"button\" class=\"btn btn-${1:$$(yas-choose-value '(\"default\" \"primary\" \"success\" \"info\" \"warning\" \"danger\" \"link\"))}\" $2>$3</button>$0" "button" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/button.yasnippet" "direct-keybinding" nil)
                       ("a" "<a href=\"$1\" class=\"$2\" $3>$4</a>$0" "<a href=\"\" class=\"\"..</a>" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/alink.yasnippet" "direct-keybinding" nil)
                       ("alert" "<div class=\"alert alert-${1:$$(yas-choose-value '(\"success\" \"info\" \"warning\" \"danger\"))}\">${2:\n<button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\">&times;</button>}\n  $0\n</div>" "alert" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/alert.yasnippet" nil nil)
                       ("a" "<a class=\"btn btn-${1:$$(yas-choose-value '(\"default\" \"primary\" \"success\" \"info\" \"warning\" \"danger\" \"link\"))}\" role=\"button\" $2>$3</a>$0" "<a class=\"btn\" ..</a>" nil
                        ("twitter-bootstrap")
                        nil "/home/chedi/.emacs.d/snippets/web-mode/abutton.yasnippet" "direct-keybinding" nil)))


;;; Do not edit! File generated at Wed Aug 26 13:15:32 2020
