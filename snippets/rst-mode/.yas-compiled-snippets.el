;;; Compiled snippets and support files for `rst-mode'
;;; Snippet definitions:
;;;
(yas-define-snippets 'rst-mode
                     '(("tit" "${1:$(make-string (string-width yas-text) ?\\=)}\n${1:Title}\n${1:$(make-string (string-width yas-text) ?\\=)}\n\n$0" "Document title" nil nil nil "/home/chedi/.emacs.d/snippets/rst-mode/tit" nil nil)
                       ("subsec" "${1:Subsection}\n${1:$(make-string (string-width yas-text) ?\\+)}\n\n" "Subsection title" nil nil nil "/home/chedi/.emacs.d/snippets/rst-mode/subsec" nil nil)
                       ("sec" "${1:Section}\n${1:$(make-string (string-width yas-text) ?\\-)}\n\n$0" "Section title" nil nil nil "/home/chedi/.emacs.d/snippets/rst-mode/sec" nil nil)
                       ("img" ".. image:: ${1:path}\n   :height: ${2:100}\n   :width: ${3:200}\n   :alt: ${4:description}\n\n$0" "image" nil nil nil "/home/chedi/.emacs.d/snippets/rst-mode/image" nil nil)
                       ("chap" "${1:Chapter}\n${1:$(make-string (string-width yas-text) ?\\=)}\n\n$0" "Chapter title" nil nil nil "/home/chedi/.emacs.d/snippets/rst-mode/chap" nil nil)))


;;; Do not edit! File generated at Wed Aug 26 13:15:32 2020
