;;; init.el --- emacs config     -*- lexical-binding: t; -*-
;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "Emacs ready in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Installing straight package manager

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(setq package-enable-at-startup nil)
(setq straight-use-package-by-default t)

(straight-use-package 'org        )
(straight-use-package 'use-package)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Setting up some emacs variables

(setq user-init-file (or load-file-name (buffer-file-name)))
(setq user-emacs-directory (file-name-directory user-init-file))
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))

(fset 'yes-or-no-p 'y-or-n-p)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; key-chord for faster keybinds

(use-package key-chord
  :ensure
  :config
    (key-chord-mode 1)
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Org-mode config

(use-package org
  :ensure
  :init
    (setq org-modules '(org-bbdb org-bibtex org-crypt org-ctags org-docview))

  :config
    (setq org-refile-targets '((nil :maxlevel . 9) (org-agenda-files :maxlevel . 9)))
    (setq org-refile-use-outline-path t)
    (setq org-outline-path-complete-in-steps nil)

    (org-babel-do-load-languages
     'org-babel-load-languages '((C . t) (python . t)))

    (setq org-babel-python-command "python")
    (setq org-latex-pdf-process '("pdflatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
    (setq org-agenda-file-regexp  "\\`[^.].*\\.org\\'")

    (setq org-directory                           (expand-file-name "~/Documents/Org/"))
    (setq org-clock-clocktable-default-properties '(:maxlevel nil :scope file))
    (setq org-enforce-todo-checkbox-dependencies  t)
    (setq org-enforce-todo-dependencies           t)
    (setq org-log-into-drawer                     t)
    (setq org-log-reschedule                      'note)

    (setq TeX-PDF-mode                t                             )
    (setq org-log-done                'time                         )
    (setq org-image-actual-width      nil                           )
    (setq org-src-fontify-natively    t                             )
    (setq org-export-latex-listings   'minted                       )
    (setq org-hide-emphasis-markers   t                             )
    (setq org-confirm-babel-evaluate  nil                           )
    (setq org-list-allow-alphabetical t                             )
    (setq org-fontify-emphasized-text t                             )

    (setq-default org-hide-leading-stars          t                              )
    (setq-default org-support-shift-select        (quote always)                 )
    (setq-default org-fontify-done-headline       t                              )
    (setq-default org-fontify-whole-heading-line  t                              )
    (setq-default org-highlight-latex-and-related (quote (latex script entities)))

    (use-package org-download
      :ensure
      :straight
        (org-download :type git :host github :repo "abo-abo/org-download")
      :custom
        (org-download-image-dir "~/Pictures/Org")
      :hook
        (org-mode   . org-download-enable)
        (dired-mode . org-download-enable)
    )

    (use-package org-bullets
      :ensure
      :config
        (add-hook 'org-mode-hook (lambda () (org-bullets-mode 1)))
    )

    (define-key org-mode-map (kbd "<f6>"        ) 'flyspell-buffer)
    (define-key org-mode-map (kbd "<C-M-return>") (lambda() (interactive) (insert-char 8626 1 t)))

    (key-chord-define org-mode-map "ww" 'org-todo                         )
    (key-chord-define org-mode-map ">>" 'org-edit-special                 )
    (key-chord-define org-mode-map "::" 'flyspell-goto-next-error         )
    (key-chord-define org-mode-map "xx" 'org-toggle-inline-images         )
    (key-chord-define org-mode-map "<<" 'org-babel-execute-and-show-images)

    (add-to-list 'auto-mode-alist '(".*/[0-9]*$" . org-mode))

    (org-clock-persistence-insinuate)
    (add-hook 'org-mode-hook 'org-indent-mode)

    (defadvice org-babel-python-evaluate-session
        (around org-python-use-cpaste (session body &optional result-type result-params) activate)
      "add a %cpaste and '--' to the body, so that ipython does the right thing."
      (setq body (concat "%cpaste -q \n" body "\n--"))
      ad-do-it)

    (defun org-babel-execute:ditaa (body params)
      "Overwriting the default or-babel-execute for the ditaa format, because fedora has a command for that"
      (let* ((result-params (split-string (or (cdr (assoc :results params)) "")))
             (out-file ((lambda (el) (or el (error "Ditaa code block requires :file header argument")))
                        (cdr (assoc :file params))))
             (cmdline (cdr (assoc :cmdline params)))
             (in-file (org-babel-temp-file "ditaa-"))
             (cmd (concat "/usr/bin/ditaa" " " cmdline " " (org-babel-process-file-name in-file) " " (org-babel-process-file-name out-file))))
        (with-temp-file in-file (insert body))
        (message cmd) (shell-command cmd)
        nil))

    (defun org-babel-execute-and-show-images ()
      (interactive)
      (org-babel-execute-src-block)
      (org-display-inline-images t t))

    (setq org-todo-keywords
          '((sequence "TODO(t)" "NEXT(n)" "WAITING(w@/!)" "IN PROGRESS(p)" "BUGGY(g@/)" "BROKEN(k@/)"
                      "|"
                      "CANCELLED(c@/!)" "DONE(d)")))

    (setq org-priority-faces '((65 . "DeepPink") (66 . "firebrick") (67 . "tomato")))

    (setq org-todo-keyword-faces
          '(("TODO"        :foreground "red"          :weight bold)
            ("NEXT"        :foreground "blue"         :weight bold)
            ("DONE"        :foreground "forest green" :weight bold)
            ("WAITING"     :foreground "orange"       :weight bold)
            ("IN PROGRESS" :foreground "orange"       :weight bold)
            ("BUGGY"       :foreground "goldnrod"     :weight bold)
            ("BROKEN"      :background "black"        :foreground "dark red" :weight bold)
            ("CANCELLED"   :foreground "forest green" :weight bold)
            ))

    (defun surround (start end txt)
      (interactive "r\nsEnter text to surround: " start end txt)
      (if (not (region-active-p))
          (let ((new-region (bounds-of-thing-at-point 'symbol)))
            (setq start (car new-region))
            (setq end (cdr new-region))))
      (let* ((s-table '(("#e" . ("#+BEGIN_EXAMPLE\n" "\n#+END_EXAMPLE") )
                        ("#s" . ("#+BEGIN_SRC \n"    "\n#+END_SRC") )
                        ("#q" . ("#+BEGIN_QUOTE\n"   "\n#+END_QUOTE"))
                        ("<"  . ("<" ">"))
                        ("("  . ("(" ")"))
                        ("{"  . ("{" "}"))
                        ("["  . ("[" "]"))))
             (s-pair (assoc-default txt s-table)))
        (unless s-pair (setq s-pair (list txt txt)))

        (save-excursion
          (narrow-to-region start end)
          (goto-char (point-min))
          (insert (car s-pair))
          (goto-char (point-max))
          (insert (cadr s-pair))
          (widen))))

    (defun surround-text (txt)
      (if (region-active-p)
          (surround (region-beginning) (region-end) txt)
        (surround nil nil txt)))

    (defun org-text-bold    () (interactive) (surround-text "*"))
    (defun org-text-code    () (interactive) (surround-text "="))
    (defun org-text-italics () (interactive) (surround-text "/"))

    (setq org-babel-python-command "python")

    (font-lock-add-keywords 'org-mode
                           '(("^ +\\([-*]\\) "
                              (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "¤"))))))

    (add-to-list 'ispell-skip-region-alist '(":\\(PROPERTIES\\|LOGBOOK\\):" . ":END:"))
    (add-to-list 'ispell-skip-region-alist '("#\\+BEGIN_SRC" . "#\\+END_SRC"))
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; rustic = basic rust-mode + additions

(use-package rustic
  :ensure
  :bind (
    :map rustic-mode-map
      ("C-c C-c a" . eglot-code-action             )
      ("C-c C-c r" . eglot-rename                  )
      ("C-c C-c e" . rustic-analyzer-macro-expand  )
      ("C-c C-c d" . dap-hydra                     )
  )
  :config
    (setq lsp-eldoc-hook                   nil                            )
    (setq rustic-lsp-server                'rust-analyzer                 )
    (setq rustic-lsp-client                'eglot                         )
    (setq rustic-format-trigger            nil                            )
    (setq rustic-format-on-save            nil                            )
    (setq rustic-analyzer-command          '("~/.local/bin/rust-analyzer"))
    (setq lsp-signature-auto-activate      nil                            )
    (setq lsp-enable-symbol-highlighting   t                              )
    (setq lsp-rust-analyzer-server-command '("~/.local/bin/rust-analyzer"))

    (add-hook 'rust-mode-hook   'eglot-ensure)
    (add-hook 'rustic-mode-hook 'eglot-ensure)
    ;; (setq rustic-lsp-format t)
    ;; comment to disable rustfmt on save
    ;; (add-hook 'rustic-mode-hook 'rk/rustic-mode-hook)
)

(defun rk/rustic-mode-hook ()
  (when buffer-file-name
    (setq-local buffer-save-without-query t))
  (add-hook 'before-save-hook 'lsp-format-buffer nil t))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; cargo manipulation

(use-package cargo
  :ensure
  :config
    (setenv "PATH" (concat (getenv "PATH") ":~/.cargo/bin"))
    (setq exec-path (append exec-path '("~/.cargo/bin")))
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; pyvenv python virtual environment

(use-package pyvenv
  :ensure
  :init
    (setenv "WORKON_HOME" "~/.virtualenvs")
    (defun pyvenv-activate-default ()
      (pyvenv-workon "default"))
  :custom
    (org-download-screenshot-method "flameshot gui --raw > %s")
  :hook
    (python-mode . pyvenv-activate-default)
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; languages server protcole (Note this will no longer be necessary in emacs 29 and later)

(use-package eglot
  :ensure
  :config
    (add-to-list 'eglot-server-programs
                 `(python-mode . ,(eglot-alternatives '(("pyright-langserver" "--stdio")))))
  :hook
    (python-mode . eglot-ensure)
    (rust-mode   . eglot-ensure)
    (rustic-mode . eglot-ensure)
)

(straight-use-package
 '(eglot-x :type git :host github :repo "nemethf/eglot-x"))
(with-eval-after-load 'eglot (require 'eglot-x))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Create / cleanup rust scratch projects quickly

(use-package rust-playground :ensure)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; for Cargo.toml and other config files

(use-package toml-mode :ensure)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Tree Sitter syntax parsing (this will be integrated in emacs 29)

(straight-use-package 'tree-sitter)
(straight-use-package 'tree-sitter-langs)

(require 'tree-sitter)
(require 'tree-sitter-langs)

(add-hook 'rust-mode-hook #'tree-sitter-mode)
(add-hook 'rust-mode-hook #'tree-sitter-hl-mode)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; setting up debugging support with dap-mode

(use-package exec-path-from-shell
  :ensure
  :init
    (exec-path-from-shell-initialize)
)

(when (executable-find "lldb-mi")
  (use-package dap-mode
    :ensure
    :config
      (dap-ui-mode)
      (dap-ui-controls-mode 1)

      (require 'dap-lldb)
      (require 'dap-gdb-lldb)
      ;; installs .extension/vscode
      (dap-gdb-lldb-setup)
      (dap-register-debug-template
       "Rust::LLDB Run Configuration"
       (list :type "lldb"
             :request "launch"
             :name "LLDB::Run"
             :gdbpath "rust-lldb"
             ;; uncomment if lldb-mi is not in PATH
             ;; :lldbmipath "path/to/lldb-mi"
             )))
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; C++ packages

(use-package irony
  :ensure
  :config
    (irony-cdb-autosetup-compile-options)
)

(use-package company-irony
  :ensure
  :config
    (eval-after-load 'company
      '(add-to-list 'company-backends 'company-irony))
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; auto-completion and code snippets

(use-package yasnippet
  :ensure
  :config
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))

(use-package company
  :ensure
  :bind
    (:map company-active-map
      ("C-n". company-select-next    )
      ("C-p". company-select-previous)
      ("M-<". company-select-first   )
      ("M->". company-select-last    )
    )
  :config
    (global-set-key (kbd "TAB") #'company-indent-or-complete-common)
  :hook
    (python-mode . company-mode)
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; some eye candies

(use-package doom-modeline
  :ensure
  :hook (after-init . doom-modeline-mode)
  :custom
    (doom-modeline-height                   25                    )
    (doom-modeline-bar-width                1                     )
    (doom-modeline-icon                     t                     )
    (doom-modeline-major-mode-icon          t                     )
    (doom-modeline-major-mode-color-icon    t                     )
    (doom-modeline-buffer-file-name-style   'truncate-upto-project)
    (doom-modeline-buffer-state-icon        t                     )
    (doom-modeline-buffer-modification-icon t                     )
    (doom-modeline-minor-modes              nil                   )
    (doom-modeline-enable-word-count        nil                   )
    (doom-modeline-buffer-encoding          t                     )
    (doom-modeline-indent-info              nil                   )
    (doom-modeline-checker-simple-format    t                     )
    (doom-modeline-vcs-max-length           12                    )
    (doom-modeline-env-version              t                     )
    (doom-modeline-irc-stylize              'identity             )
    (doom-modeline-github-timer             nil                   )
    (doom-modeline-gnus-timer               nil                   )
  :config
    (setq doom-modeline-height                 14                  )
    (setq doom-modeline-project-detection      'projectile         )
    (setq doom-modeline-env-version            t                   )
    (setq doom-modeline-buffer-file-name-style 'relative-to-project)
)

(use-package all-the-icons
  :ensure
)

(use-package doom-themes
  :ensure
  :custom
    (doom-themes-enable-bold   t)
    (doom-themes-enable-italic t)
  :config
    (doom-themes-visual-bell-config)
    (doom-themes-org-config        )
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; misc packages

(use-package vertico
  :ensure
  :init
    (vertico-mode)
  :custom
    (vertico-scroll-margin 0)
    (vertico-count 20)
    (vertico-resize nil)
    (vertico-cycle t)
    )

(use-package orderless
  :ensure
  :custom
    (completion-styles '(orderless basic))
    (completion-category-overrides '((file (styles basic partial-completion))))
)

(use-package savehist
  :ensure
  :init
    (savehist-mode)
)

(use-package consult
  ;; Replace bindings. Lazily loaded due by `use-package'.
  :bind (;; C-c bindings (mode-specific-map)
         ("C-c h" . consult-history)
         ("C-c m" . consult-mode-command)
         ("C-c k" . consult-kmacro)
         ;; C-x bindings (ctl-x-map)
         ("C-x M-:" . consult-complex-command)     ;; orig. repeat-complex-command
         ("C-x b"   . consult-buffer)              ;; orig. switch-to-buffer
         ("C-x 4 b" . consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
         ("C-x 5 b" . consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame
         ("C-x r b" . consult-bookmark)            ;; orig. bookmark-jump
         ("C-x p b" . consult-project-buffer)      ;; orig. project-switch-to-buffer
         ;; Custom M-# bindings for fast register access
         ("M-#" . consult-register-load)
         ("M-'" . consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
         ("C-M-#" . consult-register)
         ;; Other custom bindings
         ("M-y" . consult-yank-pop)                ;; orig. yank-pop
         ("<help> a" . consult-apropos)            ;; orig. apropos-command
         ;; M-g bindings (goto-map)
         ("M-g e" . consult-compile-error)
         ("M-g f" . consult-flymake)               ;; Alternative: consult-flycheck
         ("M-g g" . consult-goto-line)             ;; orig. goto-line
         ("M-g M-g" . consult-goto-line)           ;; orig. goto-line
         ("M-g o" . consult-outline)               ;; Alternative: consult-org-heading
         ("M-g m" . consult-mark)
         ("M-g k" . consult-global-mark)
         ("M-g i" . consult-imenu)
         ("M-g I" . consult-imenu-multi)
         ;; M-s bindings (search-map)
         ("M-s d" . consult-find)
         ("M-s D" . consult-locate)
         ("M-s g" . consult-grep)
         ("M-s G" . consult-git-grep)
         ("M-s r" . consult-ripgrep)
         ("M-s l" . consult-line)
         ("M-s L" . consult-line-multi)
         ("M-s m" . consult-multi-occur)
         ("M-s k" . consult-keep-lines)
         ("M-s u" . consult-focus-lines)
         ;; Isearch integration
         ("M-s e" . consult-isearch-history)
         :map isearch-mode-map
         ("M-e" . consult-isearch-history)         ;; orig. isearch-edit-string
         ("M-s e" . consult-isearch-history)       ;; orig. isearch-edit-string
         ("M-s l" . consult-line)                  ;; needed by consult-line to detect isearch
         ("M-s L" . consult-line-multi)            ;; needed by consult-line to detect isearch
         ;; Minibuffer history
         :map minibuffer-local-map
         ("M-s" . consult-history)                 ;; orig. next-matching-history-element
         ("M-r" . consult-history))                ;; orig. previous-matching-history-element

  ;; Enable automatic preview at point in the *Completions* buffer. This is
  ;; relevant when you use the default completion UI.
  :hook (completion-list-mode . consult-preview-at-point-mode)

  ;; The :init configuration is always executed (Not lazy)
  :init

  ;; Optionally configure the register formatting. This improves the register
  ;; preview for `consult-register', `consult-register-load',
  ;; `consult-register-store' and the Emacs built-ins.
  (setq register-preview-delay 0.5
        register-preview-function #'consult-register-format)

  ;; Optionally tweak the register preview window.
  ;; This adds thin lines, sorting and hides the mode line of the window.
  (advice-add #'register-preview :override #'consult-register-window)

  ;; Use Consult to select xref locations with preview
  (setq xref-show-xrefs-function #'consult-xref
        xref-show-definitions-function #'consult-xref)

  ;; Configure other variables and modes in the :config section,
  ;; after lazily loading the package.
  :config

  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.
  (consult-customize
   consult-theme :preview-key '(:debounce 0.2 any)
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-file-register
   consult--source-recent-file consult--source-project-recent-file
   ;; :preview-key (kbd "M-.")
   :preview-key '(:debounce 0.4 any))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; By default `consult-project-function' uses `project-root' from project.el.
  ;; Optionally configure a different project root function.
  ;; There are multiple reasonable alternatives to chose from.
  ;;;; 1. project.el (the default)
  ;; (setq consult-project-function #'consult--default-project--function)
  ;;;; 2. projectile.el (projectile-project-root)
  ;; (autoload 'projectile-project-root "projectile")
  ;; (setq consult-project-function (lambda (_) (projectile-project-root)))
  ;;;; 3. vc.el (vc-root-dir)
  ;; (setq consult-project-function (lambda (_) (vc-root-dir)))
  ;;;; 4. locate-dominating-file
  ;; (setq consult-project-function (lambda (_) (locate-dominating-file "." ".git")))
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; misc packages

(use-package which-key
  :ensure
  :init
  (which-key-mode)
)

(use-package marginalia
  :bind (
         ("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle)
  )
  :init
    (marginalia-mode)
)

(use-package embark
  :ensure
  :straight nil
  :bind (
         ("C-."   . embark-act)       ;; pick some comfortable binding
         ("C-;"   . embark-dwim)      ;; good alternative: M-.
         ("C-h B" . embark-bindings)  ;; alternative for `describe-bindings'
  )
  :init
    ;; Optionally replace the key help with a completing-read interface
    (setq prefix-help-command #'embark-prefix-help-command)
  :config
    ;; Hide the mode line of the Embark live/completions buffers
    (add-to-list 'display-buffer-alist
                 '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
                   nil
                   (window-parameters (mode-line-format . none))))
)

(use-package embark-consult
  :ensure
  :straight nil
  :hook
    (embark-collect-mode . consult-preview-at-point-mode)
)

(use-package dirvish
  :ensure
  :config
    (dirvish-override-dired-mode)
)

(use-package whole-line-or-region
  :ensure
  :config
    (whole-line-or-region-global-mode)
)

(use-package expand-region
  :ensure
  :bind (
    ("C-M-SPC" . er/expand-region  )
    ("C-="     . er/contract-region)
  )
)

(use-package unicode-fonts
  :ensure
  :config
    (unicode-fonts-setup)
)

(use-package whitespace-cleanup-mode
  :ensure
  :diminish whitespace-cleanup-mode
  :init
  (setq whitespace-cleanup-mode-only-if-initially-clean nil
        whitespace-line-column 80
        whitespace-style '(face lines-tail))
  :config
    (global-whitespace-mode)
    (global-whitespace-cleanup-mode)
)

(use-package wrap-region
  :ensure
  :config
  (wrap-region-add-wrappers
   '(("*" "*" nil org-mode)
     ("~" "~" nil org-mode)
     ("/" "/" nil org-mode)
     ("=" "=" nil org-mode)
     ("_" "_" nil org-mode)
     ("$" "$" nil (org-mode latex-mode))))
  (add-hook 'org-mode-hook 'wrap-region-mode)
  (add-hook 'latex-mode-hook 'wrap-region-mode))

(use-package desktop+
  :ensure
  :init
  (setq desktop-restore-frames t)
  (setq desktop-dirname           "~/.emacs.d/desktop/"
      desktop-base-file-name      "emacs.desktop"
      desktop-base-lock-name      "lock"
      desktop-path                (list desktop-dirname)
      desktop-save                t
      desktop-files-not-to-save   "^$" ;reload tramp paths
      desktop-load-locked-desktop nil)

  :config
  (if (not (daemonp))
      (desktop-save-mode 1)
    (defun restore-desktop (frame)
      "Restores desktop and cancels hook after first frame opens.
     So the daemon can run at startup and it'll still work"
      (with-selected-frame frame
        (desktop-save-mode 1)
        (desktop-read)
        (remove-hook 'after-make-frame-functions 'restore-desktop)))
    (add-hook 'after-make-frame-functions 'restore-desktop))
  (desktop-save-mode 1))

(use-package bm
  :ensure
  :config
  (setq bm-cycle-all-buffers t)

  :bind (
         ("<f7>"        . bm-toggle  )
         ("<C-M-next>"  . bm-next    )
         ("<C-M-prior>" . bm-previous)))

(use-package aggressive-indent   :ensure)
(use-package yasnippet-snippets  :ensure)
(use-package magit               :ensure)
(use-package forge               :ensure)
(use-package vterm               :ensure)
(use-package multiple-cursors    :ensure)
(use-package google-this         :ensure)
(use-package find-temp-file      :ensure)
(use-package multi-vterm         :ensure)
(use-package avy                 :ensure)
(use-package git-gutter+         :ensure)

(straight-use-package
 '(buffer-move :type git :host github :repo "lukhas/buffer-move"))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Comment editing

(use-package separedit
  :ensure
  :straight (separedit :type git :host github :repo "twlz0ne/separedit.el")
  :bind (
    ("C-c :" . separedit)
  )
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Code folding using tree sitter

(use-package ts-fold
  :ensure
  :straight (ts-fold :type git :host github :repo "emacs-tree-sitter/ts-fold")
  :bind (
          ("s-="               . ts-fold-open     )
          ("s-<kp-add>"        . ts-fold-open     )
          ("s--"               . ts-fold-close    )
          ("s-<kp-subtract>"   . ts-fold-close    )
          ("M-s-="             . ts-fold-open-all )
          ("M-s-<kp-add>"      . ts-fold-open-all )
          ("M-s--"             . ts-fold-close-all)
          ("M-s-<kp-subtract>" . ts-fold-close-all)))

(use-package highlight-indent-guides
  :ensure
  :hook
     (prog-mode-hook . highlight-indent-guides-mode)
  :custom
     (highlight-indent-guides-method    'character)
     (highlight-indent-guides-character 9475      )
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; various settings

(use-package projectile
  :ensure
  :bind (
    ("<s-end>"  . next-code-buffer    )
    ("<s-home>" . previous-code-buffer)
  )

  :config
    (setq projectile-enable-caching               nil                                                    )
    (setq projectile-require-project-root         nil                                                    )
    (setq projectile-project-root-files-bottom-up (delete ".git" projectile-project-root-files-bottom-up))
    (setq projectile-project-root-files-functions
      '(projectile-root-local
        projectile-root-top-down
        projectile-root-bottom-up))

    (global-set-key [remap next-buffer    ] 'project-next-code-buffer    )
    (global-set-key [remap previous-buffer] 'project-previous-code-buffer)

    (add-to-list 'projectile-project-root-files ".git")
    (advice-add 'projectile-get-repo-files :override #'advice-projectile-no-sub-project-files)
    (projectile-global-mode)
)

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; various settings

(auto-insert-mode  )
(electric-pair-mode)

(add-hook 'term-mode-hook (lambda() (setq yas-dont-activate t)))
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(winner-mode              1)
(hl-line-mode             1)
(menu-bar-mode           -1)
(tool-bar-mode           -1)
(yas-global-mode          1)
(show-paren-mode          1)
(scroll-bar-mode         -1)
(display-time-mode        1)
(column-number-mode       1)
(transient-mark-mode      1)
(global-company-mode      1)
(delete-selection-mode    1)
(global-auto-revert-mode  1)

(setq whitespace-style (quote (face spaces tabs newline space-mark tab-mark)))
(setq whitespace-display-mappings
    '((space-mark   32 [183    ] [46  ])
      (newline-mark 10 [8629 10]       )
      (tab-mark      9 [8594  9] [92 9])))

(setq-default tab-width        4  )
(setq-default fill-column      100)
(setq-default truncate-lines   t  )
(setq-default indent-tabs-mode nil)

(setq tab-bar-show                 nil        )
(setq fci-rule-color               "lightgray")
(setq yas/indent-line              nil        )
(setq backup-inhibited             t          )
(setq dired-dwim-target            t          )
(setq inhibit-splash-screen        t          )
(setq whitespace-line-column       100        )
(setq inhibit-startup-message      t          )
(setq enable-recursive-minibuffers t          )

(setq whitespace-global-modes '(not org-mode))

(setq global-auto-revert-non-file-buffers      t                     )
(setq native-comp-async-report-warnings-errors 'silent               )
(setq bmkp-last-as-first-bookmark-file         "~/.emacs.d/bookmarks")
(setq browse-url-browser-function              'browse-url-firefox   )
(setq display-time-24hr-format                 t                     )
(setq display-time-day-and-date                t                     )
(setq display-time-mode                        t                     )

(setq python-shell-interpreter "ipython")

(setq max-mini-window-height 1)
(setq global-auto-revert-non-file-buffers t)

(put 'scroll-left               'disabled nil)
(put 'upcase-region             'disabled nil)
(put 'downcase-region           'disabled nil)
(put 'narrow-to-page            'disabled nil)
(put 'narrow-to-region          'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

(add-hook 'emacs-lisp-mode-hook (lambda () (prettify-symbols-mode)))

(setq byte-compile-warnings '(not callargs redefine obsolete nresolved free-vars
                                  noruntime cl-functions interactive-only))

(defadvice save-buffers-kill-emacs (around no-query-kill-emacs activate)
  (cl-letf (((symbol-function #'process-list) (lambda ())))
    ad-do-it))

(eval-after-load "linum"
  '(set-face-attribute 'linum nil :height 120))

;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Helper functions

(defun move-text-internal (arg)
;;;moving lines up and down
  (cond
   ((and mark-active transient-mark-mode)
    (if (> (point) (mark))
        (exchange-point-and-mark))
    (let ((column (current-column))
          (text (delete-and-extract-region (point) (mark))))
      (forward-line arg)
      (move-to-column column t)
      (set-mark (point))
      (insert text)
      (exchange-point-and-mark)
      (setq deactivate-mark nil)))
   (t
    (beginning-of-line)
    (when (or (> arg 0) (not (bobp)))
      (forward-line)
      (when (or (< arg 0) (not (eobp)))
        (transpose-lines arg))
      (forward-line -1)))))

(defun move-text-down (arg)
  "Move region (transient-mark-mode active) or current line arg lines down."
  (interactive "*p")
  (move-text-internal arg))

(defun move-text-up (arg)
  "Move region (transient-mark-mode active) or current line arg lines up."
  (interactive "*p")
  (move-text-internal (- arg)))

(defun duplicate-current-line-or-region (arg)
  "Duplicates the current line or region ARG times.
   If there's no region, the current line will be duplicated. However, if
   there's a region, all lines that region covers will be duplicated."
  (interactive "p")
  (let (beg end (origin (point)))
    (if (and mark-active (> (point) (mark)))
        (exchange-point-and-mark))
    (setq beg (line-beginning-position))
    (if mark-active
        (exchange-point-and-mark))
    (setq end (line-end-position))
    (let ((region (buffer-substring-no-properties beg end)))
      (dotimes (i arg)
        (goto-char end)
        (newline)
        (insert region)
        (setq end (point)))
      (goto-char (+ origin (* (length region) arg) arg)))))

(defun move-line-up ()
  "Move up the current line."
  (interactive)
  (transpose-lines 1)
  (forward-line -2)
  (indent-according-to-mode))

(defun move-line-down ()
  "Move down the current line."
  (interactive)
  (forward-line 1)
  (transpose-lines 1)
  (forward-line -1)
  (indent-according-to-mode))

(defun toggle-comment-on-line ()
  "comment or uncomment current line"
  (interactive)
  (comment-or-uncomment-region (line-beginning-position) (line-end-position)))

(defun remove-nth-element (nth list)
  (if (zerop nth) (cdr list)
    (let ((last (nthcdr (1- nth) list)))
      (setcdr last (cddr last))
      list)))

(defun my-insert-rectangle-push-lines ()
  "Yank a rectangle as if it was an ordinary kill."
  (interactive "*")
  (when (and (use-region-p) (delete-selection-mode))
    (delete-region (region-beginning) (region-end)))
  (save-restriction
    (narrow-to-region (point) (mark))
    (yank-rectangle)))

(defun end-of-line-and-indented-new-line ()
  (interactive)
  (end-of-line)
  (newline-and-indent))

(defun ct/kill-to-beginning-of-line-dwim ()
  "Kill from point to beginning of line.

In `prog-mode', delete up to beginning of actual, not visual
line, stopping at whitespace. Repeat to delete whitespace. In
other modes, e.g. when editing prose, delete to beginning of
visual line only."
  (interactive)
  (let ((current-point (point)))
      (if (not (derived-mode-p 'prog-mode))
          ;; In prose editing, kill to beginning of (visual) line.
          (if visual-line-mode
              (kill-visual-line 0)
            (kill-line 0))
        ;; When there's whitespace at the beginning of the line, go to
        ;; before the first non-whitespace char.
        (beginning-of-line)
        (when (search-forward-regexp (rx (not space)) (point-at-eol) t)
          ;; `search-forward' puts point after the find, i.e. first
          ;; non-whitespace char. Step back to capture it, too.
          (backward-char))
        (kill-region (point) current-point))))

(defun rename-file-and-buffer (new-name)
  "Rename both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file name new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modifier-p nil)
          )))))

(defun delete-this-buffer-and-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun insert-tab ()
  "Insert a tab char."
  (interactive)
  (insert-char 9))

(defun insert-right-arrow ()
  "Insert -> ."
  (interactive)
  (insert-string "->"))

(defun insert-left-arrow ()
  "Insert <- ."
  (interactive)
  (insert-string "<-"))

(defun insert-current-timestamp ()
  "Spit out the current time in Y-m-d format."
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))

(defun start-bash ()
  "Start a bash terminal."
  (interactive)
  (ansi-term "/bin/bash"))

(defun advice-projectile-no-sub-project-files ()
  (projectile-files-via-ext-command (projectile-get-ext-command)))

(defun project-next-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'next-buffer t))

(defun project-previous-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'previous-buffer t))

(defun next-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'next-buffer nil))

(defun previous-code-buffer ()
  (interactive)
  (move-to-project-code-buffer 'previous-buffer nil))

(defun move-to-project-code-buffer (move-handler restrict-to-project)
  (require 'projectile)
  (let ((original_buffer_name (buffer-name))
        (exausted_buffers nil)
        (project_buffers_names
         (when (projectile-project-p)
           (mapcar 'buffer-name (projectile-project-buffers-non-visible)))
         ))
    (when (or project_buffers_names (not restrict-to-project))
      (funcall move-handler)
      (while (and (not exausted_buffers)
                  (or (string-match-p "^\*" (buffer-name))
                      (and project_buffers_names restrict-to-project
                           (not (member (buffer-name) project_buffers_names)))))
        (if (not (equal original_buffer_name (buffer-name)))
            (funcall move-handler)
          (setq exausted_buffers t))))))

(defun sort-lines-by-length (reverse beg end)
  "Sort lines by length."
  (interactive "P\nr")
  (save-excursion
    (save-restriction
      (narrow-to-region beg end)
      (goto-char (point-min))
      (let ;; To make `end-of-line' and etc. to ignore fields.
          ((inhibit-field-text-motion t))
        (sort-subr reverse 'forward-line 'end-of-line nil nil
                   (lambda (l1 l2)
                     (apply #'< (mapcar (lambda (range) (- (cdr range) (car range)))
                                        (list l1 l2)))))))))

(defun my-kill-thing-at-point (thing)
  "Kill the `thing-at-point' for the specified kind of THING."
  (let ((bounds (bounds-of-thing-at-point thing)))
    (if bounds
        (kill-region (car bounds) (cdr bounds))
      (error "No %s at point" thing))))

(defun my-kill-word-at-point ()
  "Kill the word at point."
  (interactive)
  (my-kill-thing-at-point 'word))


;; -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
;; Shortcuts

(keyboard-translate ?\C-i ?\H-i)
(unbind-key "C-M-i" emacs-lisp-mode-map)

(require 'term)
(require 'vterm)
(require 'cua-base)
(require 'projectile)

(key-chord-define-global "éé" 'mark-whole-buffer             )
(key-chord-define-global "xs" 'save-buffer                   )
(key-chord-define-global "²&" 'find-file                     )
(key-chord-define-global "&é" 'consult-buffer-other-window   )
(key-chord-define-global "&&" 'consult-locate                )
(key-chord-define-global "²²" 'consult-find                  )
(key-chord-define-global "hh" 'magit-log-buffer-file         )
(key-chord-define-global "<w" 'consult-project-buffer        )

(key-chord-define-global "$$" 'mark-sexp               )
(key-chord-define-global "**" 'mc/mark-all-like-this   )
(key-chord-define-global "gt" 'goto-line               )
(key-chord-define-global "@@" 'find-temp-file          )
(key-chord-define-global "aa" 'align-regexp            )
(key-chord-define-global "qq" 'magit-status            )
(key-chord-define-global ",," 'pop-to-mark-command     )
(key-chord-define-global "ww" (lambda () (interactive) (insert "->")))
(key-chord-define-global "WW" (lambda () (interactive) (insert "<-")))
(key-chord-define-global "!!" (lambda () (interactive) (insert "=>")))
(key-chord-define-global "§§" (lambda () (interactive) (insert "<=")))
(key-chord-define-global "xx" 'mc/vertical-align-with-space)

(key-chord-define lisp-mode-map "cd" 'eval-last-sexp)

(global-set-key (kbd "<f6>"     ) 'findr-query-replace  )
(global-set-key (kbd "<f11>"    ) 'eglot-x-open-external-documentation)
(global-set-key (kbd "<C-f2>"   ) 'browse-url           )
(global-set-key (kbd "<C-f5>"   ) (lambda () (interactive) (revert-buffer t t nil)))
(global-set-key (kbd "<M-s-SPC>") 'pop-to-mark-command )
(global-set-key (kbd "<M-SPC>"  ) 'just-one-space      )
(global-set-key (kbd "<M-escape> <M-escape>") 'helm-M-x )

(global-set-key (kbd "<C-escape>") 'tab-bar-rename-tab        )
(global-set-key (kbd "s-<escape>") 'tab-bar-new-tab           )
(global-set-key (kbd "<M-tab>"   ) 'tab-bar-switch-to-next-tab)
(global-set-key (kbd "M-<iso-lefttab>" ) 'tab-bar-switch-to-prev-tab)
(global-set-key (kbd "<C-next>"  ) 'tab-bar-switch-to-next-tab)
(global-set-key (kbd "<C-prior>" ) 'tab-bar-switch-to-prev-tab)

(define-key term-mode-map       (kbd "C-²") 'term-toggle-mode)
(define-key term-raw-map        (kbd "C-²") 'term-toggle-mode)
(define-key vterm-mode-map      (kbd "C-²") 'vterm-copy-mode)
(define-key vterm-copy-mode-map (kbd "C-²") 'vterm-copy-mode)

(global-set-key (kbd "<f5>"   ) 'google-this)
(global-set-key (kbd "<f12>"  ) 'rectangle-mark-mode)
(global-set-key (kbd "<C-f12>") 'multi-vterm)

(global-set-key (kbd "C-*"      ) 'kill-word             )
(global-set-key (kbd "C-s-*"    ) 'align-regexp          )
(global-set-key (kbd "C-:"      ) 'toggle-comment-on-line)
(global-set-key (kbd "C-!"      ) 'kill-this-buffer      )
(global-set-key (kbd "s-!"      ) 'kill-buffer-and-window)
(global-set-key (kbd "<M-f4>"   ) 'tab-bar-close-tab     )
(global-set-key (kbd "<M-S-f4>" ) 'tab-bar-undo-close-tab)
(global-set-key (kbd "<s-next>" ) 'next-buffer           )
(global-set-key (kbd "<s-prior>") 'previous-buffer       )
(global-set-key (kbd "<C-tab>"  ) 'next-buffer           )
(global-set-key (kbd "<C-S-tab>") 'previous-buffer       )
(global-set-key (kbd "s-<next>" ) 'next-buffer           )
(global-set-key (kbd "s-<prior>") 'previous-buffer       )

(global-set-key (kbd "<M-f3>") 'mc/mark-all-like-this)
(global-set-key (kbd "C-S-<down>") 'mc/mark-next-like-this)
(global-set-key (kbd "C-s-,") 'mc/mark-next-like-this)
(global-set-key (kbd "C-S-<up>") 'mc/mark-previous-like-this  )
(global-set-key (kbd "C-s-;") 'mc/mark-previous-like-this  )
(global-set-key (kbd "C-s-:") 'mc/unmark-next-like-this   )
(global-set-key (kbd "C-s-!") 'mc/unmark-previous-like-this)
(global-set-key (kbd "<C-S-kp-subtract>") 'mc/unmark-next-like-this)

(global-set-key (kbd "M-²") 'avy-goto-char-timer)
(global-set-key (kbd "M-&") 'avy-goto-char-2)
(global-set-key (kbd "M-&") 'avy-goto-word-or-subword-1)

(global-set-key (kbd "s-&") 'tab-bar-switch-to-tab )
(global-set-key (kbd "s-:") 'comment-region)

(global-set-key (kbd "<f8>"  ) 'flymake-goto-next-error)
(global-set-key (kbd "<C-f8>") 'flymake-goto-prev-error)
(global-set-key (kbd "<f9>"  ) 'flymake-show-buffer-diagnostics)
(global-set-key (kbd "<C-f9>") 'flymake-show-project-diagnostics)

(global-set-key (kbd "C-+") 'text-scale-increase)
(global-set-key (kbd "<C-s-kp-add>") 'text-scale-increase)
(global-set-key (kbd "C--") 'text-scale-decrease)
(global-set-key (kbd "<C-s-kp-subtract>") 'text-scale-decrease)

(global-set-key (kbd "C-s-b") 'kill-emacs)
(global-set-key (kbd "<s-delete>") 'delete-this-buffer-and-file)

(global-set-key (kbd "C-s-t") 'find-temp-file)

(global-set-key (kbd "<M-up>"   ) 'move-line-up)
(global-set-key (kbd "<M-down>"   ) 'move-line-down)
(global-set-key (kbd "<C-s-up>"   ) 'move-text-up)
(global-set-key (kbd "<C-s-down>"  ) 'move-text-down)
(global-set-key (kbd "C-<") (lambda () (interactive) (insert "<T>")))
(global-set-key (kbd "<C-M-tab>"   ) 'insert-tab)
(global-set-key (kbd "s-d") 'duplicate-current-line-or-region)
(global-set-key (kbd "C-s-o") 'find-file-in-project)
(global-set-key (kbd "C-s-s") 'desktop-save-in-desktop-dir)

(global-set-key (kbd "<C-f3>") 'rgrep)
(global-set-key (kbd "<C-f4>") 'find-name-dired)
(global-set-key (kbd "C-;") 'pop-tag-mark)

(global-set-key (kbd "M-X") 'smex-major-mode-commands)
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

(global-set-key (kbd "<M-s-right>") 'sp-forward-symbol )
(global-set-key (kbd "<M-s-left>" ) 'sp-backward-symbol)

(global-set-key (kbd "C-s-r") 'rename-file-and-buffer)
(global-set-key (kbd "C-s-t") 'insert-current-timestamp)

(global-set-key (kbd "<C-s-tab>") 'tabify              )
(global-set-key (kbd "C-M-="    ) 'indent-region       )

(global-set-key (kbd "<M-s-tab>"      ) 'hs-toggle-hiding )
(global-set-key (kbd "<M-C-return>"   ) 'hs-toggle-hiding )
(global-set-key (kbd "<M-s-return>"   ) 'hs-hide-all      )
(global-set-key (kbd "<M-s-backspace>") 'hs-show-all      )

(global-set-key [escape] 'keyboard-escape-quit)

(global-set-key (kbd "M-j") 'backward-word)
(global-set-key (kbd "M-i") 'backward-sexp)
(global-set-key (kbd "M-k") 'forward-sexp)
(global-set-key (kbd "M-l") 'forward-word)

(global-set-key (kbd "C-M-j") 'paredit-backward)
(global-set-key (kbd "C-M-l") 'paredit-forward)
(global-set-key (kbd "C-M-k") 'paredit-forward-down)
(global-set-key (kbd "C-M-i") 'paredit-backward-up)

(global-set-key (kbd "C-*") 'delete-forward-char)
(global-set-key (kbd "C-%") 'kill-word)
(global-set-key (kbd "C-$") 'kill-line)
(global-set-key (kbd "<C-dead-circumflex>") 'backward-delete-char-untabify)
(global-set-key (kbd "<C-dead-diaeresis>") 'backward-kill-word)

(global-set-key (kbd "<M-o>"   ) 'open-previous-line)
(global-set-key (kbd "<f2>"    ) 'eglot-rename)
(global-set-key (kbd "<M-f1>"  ) 'help-command)
(global-set-key (kbd "<M-f11>" ) (lambda () (interactive) (switch-to-buffer "*Messages*")))
(global-set-key (kbd "<M-f2>"  ) (lambda () (interactive) (switch-to-buffer "*scratch*" )))

(global-set-key (kbd "C-a"            ) 'move-beginning-of-line)
(global-set-key (kbd "<C-S-backspace>") 'kill-whole-line       )
(global-set-key (kbd "<M-return>"     ) (lambda() (interactive) (previous-line) (move-end-of-line nil) (electric-newline-and-maybe-indent)))
(global-set-key (kbd "<S-return>"     ) (lambda() (interactive) (move-end-of-line nil) (electric-newline-and-maybe-indent)))
(global-set-key (kbd "<C-s-M-return>" ) 'magit-diff-buffer-file)

(define-key cua-global-keymap (kbd "<C-return>") 'nil)
(define-key cua-global-keymap (kbd "<C-escape>") 'cua-set-rectangle-mark)

(global-set-key (kbd "<C-S-prior>") 'move-line-up       )
(global-set-key (kbd "<C-S-next>" ) 'move-line-down     )
(global-set-key (kbd "<C-s-next>" ) 'yas/next-field     )
(global-set-key (kbd "<C-s-prior>") 'yas/prev-field     )
(global-set-key (kbd "<M-s-next>" ) 'narrow-to-region   )
(global-set-key (kbd "<M-s-prior>") 'narrow-to-page     )

(global-set-key (kbd "<C-s-return>") 'auto-insert)

(global-set-key (kbd "C-c C-u") 'string-inflection-all-cycle)

(global-set-key (kbd "C-z"  ) 'undo               )
(global-set-key (kbd "C-S-z") 'undo-tree-redo     )
(global-set-key (kbd "C-M-z") 'undo-tree-visualize)

;; Window/Buffers
(global-set-key (kbd "s-i"    ) 'windmove-up   )
(global-set-key (kbd "s-<up>"   ) 'windmove-up   )
(global-set-key (kbd "s-<kp-5>" ) 'windmove-up   )

(global-set-key (kbd "s-j"    ) 'windmove-left )
(global-set-key (kbd "s-<left>" ) 'windmove-left )
(global-set-key (kbd "s-<kp-1>" ) 'windmove-left )

(global-set-key (kbd "s-k"    ) 'windmove-down )
(global-set-key (kbd "s-<down>" ) 'windmove-down )
(global-set-key (kbd "s-<kp-2>" ) 'windmove-down )

(global-set-key (kbd "s-l"    ) 'windmove-right)
(global-set-key (kbd "s-<right>") 'windmove-right)
(global-set-key (kbd "s-<kp-3>" ) 'windmove-right)

(global-set-key (kbd "<C-M-s-home>"  ) 'buf-move-up   )
(global-set-key (kbd "<C-M-s-end>"   ) 'buf-move-down )
(global-set-key (kbd "<C-M-s-next>"  ) 'buf-move-right)
(global-set-key (kbd "<C-M-s-delete>") 'buf-move-left )

(global-set-key (kbd "M-s-i") 'buf-move-up   )
(global-set-key (kbd "M-s-k") 'buf-move-down )
(global-set-key (kbd "M-s-l") 'buf-move-right)
(global-set-key (kbd "M-s-j") 'buf-move-left )

(global-set-key (kbd "s-<f4>") 'delete-window       )
(global-set-key (kbd "s-<f3>") 'split-window-below  )
(global-set-key (kbd "s-<f2>") 'split-window-right  )
(global-set-key (kbd "s-<f1>") 'delete-other-windows)

(global-set-key (kbd "C-s-<f3>") (lambda () (interactive) (split-window-below) (multi-vterm)))
(global-set-key (kbd "C-s-<f2>") (lambda () (interactive) (split-window-right) (multi-vterm)))

(global-set-key (kbd "C-M-s-i"   ) 'enlarge-window)
(global-set-key (kbd "<C-M-s-up>") 'enlarge-window)

(global-set-key (kbd "C-M-s-k"      ) 'shrink-window)
(global-set-key (kbd "<C-M-s-down>" ) 'shrink-window)

(global-set-key (kbd "C-M-s-l"      ) 'enlarge-window-horizontally)
(global-set-key (kbd "<C-M-s-right>") 'enlarge-window-horizontally)

(global-set-key (kbd "C-M-s-j"      ) 'shrink-window-horizontally )
(global-set-key (kbd "<C-M-s-left>" ) 'shrink-window-horizontally )

(global-set-key (kbd "M-s-y") 'yank-rectangle)
(global-set-key (kbd "C-s-y") 'my-insert-rectangle-push-lines)

(global-set-key (kbd "C-M-s-<backspace>") 'erase-buffer)
(global-set-key (kbd "C-<return>") 'end-of-line-and-indented-new-line)
(global-set-key (kbd "C-c C-d") (lambda () (interactive) (undo-tree-discard-history)))

(global-set-key (kbd "M-<backspace>") #'ct/kill-to-beginning-of-line-dwim)
(global-set-key (kbd "s-<backspace>") 'my-kill-word-at-point)

(global-set-key (kbd "C-s-<f1>") 'make-frame-command)
(global-set-key (kbd "C-s-<f4>") 'delete-frame)
